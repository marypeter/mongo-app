package com.niit.myrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongExampleApplication.class, args);
	}

}
