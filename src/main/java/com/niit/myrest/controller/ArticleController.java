package com.niit.myrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.niit.myrest.model.Article;
import com.niit.myrest.service.AritcleDao;

@RestController
@RequestMapping("api/mongo")
public class ArticleController {

	
	@Autowired
	AritcleDao artdao;
	
	@RequestMapping(method=RequestMethod.POST,value="/addrec")
	
	public ResponseEntity<?> addRec(@RequestBody Article article)
	{
		Article obj=artdao.addArticle(article);
		return new ResponseEntity<Article>(obj,HttpStatus.OK);
	}
	
	@GetMapping("/showall")
	
	public ResponseEntity<?> showall()
	{
		List li=artdao.showArticle();
		return new ResponseEntity<List>(li,HttpStatus.OK);
	}
}
